package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.ActivityTransactionDto;

public interface ActivityTransactionService {

	ActivityTransactionDto createActivityTransaction(ActivityTransactionDto activityTransactionDto);

	ActivityTransactionDto getActivityTransactionById(Long id);

	List<ActivityTransactionDto> getActivityTransactions();

	void deleteActivityTransactionById(Long id);

}
