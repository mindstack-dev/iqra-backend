package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.ActivityDto;

public interface ActivityService {

	ActivityDto createActivity(ActivityDto activityDto);

	ActivityDto getActivityById(Long id);

	List<ActivityDto> getAllActivities();

	void deleteActivity(Long id);

}
