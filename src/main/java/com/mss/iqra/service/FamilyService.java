package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.FamilyDto;

public interface FamilyService {

	FamilyDto createFamily(FamilyDto familyDto);

	FamilyDto getFamilyById(Long id);

	List<FamilyDto> getAllFamilies();

	void deleteFamily(Long id);

}
