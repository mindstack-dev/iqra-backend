package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.LoginDto;
import com.mss.iqra.dto.UserDto;

public interface UserService {

	UserDto createUser(UserDto userDto);

	UserDto getUserById(Long id);

	List<UserDto> getAllUsers();

	void deleteUserById(Long id);
	
	UserDto loginUser(LoginDto loginDto) throws Exception;

}
