package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.FamilyScoreDto;

public interface FamilyScoreService {
	
	FamilyScoreDto createFamilyScore(FamilyScoreDto familyScoreDto);
	
	FamilyScoreDto getFamilyScore(Long id);
	
	List<FamilyScoreDto> getFamilyScoreList();
	
	void deleteFamilyScoreById(Long id);

}
