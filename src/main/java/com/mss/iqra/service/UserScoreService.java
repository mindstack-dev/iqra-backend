package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.UserScoreDto;

public interface UserScoreService {

	UserScoreDto createUserScore(UserScoreDto userScoreDto);

	UserScoreDto getUserScoreById(Long id);

	List<UserScoreDto> getUserScoreList();

	void deleteUserScoreById(Long id);

}
