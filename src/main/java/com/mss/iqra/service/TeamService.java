package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.TeamDto;

public interface TeamService {
	
	TeamDto createTeam(TeamDto teamDto);
	
	TeamDto getTeamById(Long id);
	
	List<TeamDto> getAllTeams();
	
	void deleteTeam(Long id);

}
