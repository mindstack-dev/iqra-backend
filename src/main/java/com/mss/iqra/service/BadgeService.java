package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.BadgeDto;

public interface BadgeService {
	
	BadgeDto createBadge(BadgeDto badgeDto);
	
	BadgeDto getBadgeById(Long id);
	
	List<BadgeDto> getBadgeList();
	
	void deleteBadgeById(Long id);

}
