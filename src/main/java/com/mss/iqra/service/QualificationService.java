package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.QualificationDto;

public interface QualificationService {
	
	QualificationDto createQualification(QualificationDto qualificationDto);
	
	QualificationDto getQualificationById(Long id);
	
	List<QualificationDto> getAllQualifications();
	
	void deleteQualification(Long id);

}
