package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.DashboardDto;

public interface DashboardService {
	
	DashboardDto createDashboard(DashboardDto dashboardDto);
	
	DashboardDto getDashboardById(Long id);
	
	List<DashboardDto> getDashboardList();
	
	void deleteDashboardById(Long id);

}
