package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.UserScoreDto;
import com.mss.iqra.model.UserScore;
import com.mss.iqra.repo.UserScoreRepo;
import com.mss.iqra.service.UserScoreService;

@Service
public class UserScoreServiceImpl implements UserScoreService {

	@Autowired
	UserScoreRepo userScoreRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public UserScoreDto createUserScore(UserScoreDto userScoreDto) {
		UserScore userScore = dozerBeanMapper.map(userScoreDto, UserScore.class);
		return dozerBeanMapper.map(userScoreRepo.save(userScore), UserScoreDto.class);
	}

	@Override
	public UserScoreDto getUserScoreById(Long id) {

		return dozerBeanMapper.map(userScoreRepo.findById(id), UserScoreDto.class);
	}

	@Override
	public List<UserScoreDto> getUserScoreList() {
		List<UserScore> userScoreList = userScoreRepo.findAll();
		return userScoreList.stream().map(g -> dozerBeanMapper.map(g, UserScoreDto.class)).collect(Collectors.toList());
	}

	@Override
	public void deleteUserScoreById(Long id) {

		userScoreRepo.deleteById(id);

	}

}
