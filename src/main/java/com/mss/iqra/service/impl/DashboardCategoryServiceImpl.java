package com.mss.iqra.service.impl;

import java.util.List;

import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.DashboardCategoryDto;
import com.mss.iqra.model.DashboardCategory;
import com.mss.iqra.repo.DashboardCategoryRepo;
import com.mss.iqra.service.DashboardCategoryService;

@Service
public class DashboardCategoryServiceImpl implements DashboardCategoryService {

	@Autowired
	DashboardCategoryRepo dashboardCategoryRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public DashboardCategoryDto createDashboardCategory(DashboardCategoryDto dashboardCategoryDto) {
		DashboardCategory dashboardCategory = dozerBeanMapper.map(dashboardCategoryDto, DashboardCategory.class);
		return dozerBeanMapper.map(dashboardCategoryRepo.save(dashboardCategory), DashboardCategoryDto.class);
	}

	@Override
	public DashboardCategoryDto getDashboardById(Long id) {

		return dozerBeanMapper.map(dashboardCategoryRepo.findById(id), DashboardCategoryDto.class);
	}

	@Override
	public List<DashboardCategoryDto> getDashboardList() {
		List<DashboardCategory> dashboardCategoryList = dashboardCategoryRepo.findAll();
		return dashboardCategoryList.stream().map(g -> dozerBeanMapper.map(g, DashboardCategoryDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public void deleteDashboardCategoryById(Long id) {

		dashboardCategoryRepo.deleteById(id);

	}

}
