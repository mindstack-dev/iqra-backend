package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.ActivityTransactionDto;
import com.mss.iqra.model.ActivityTransaction;
import com.mss.iqra.repo.ActivityTransactionRepo;
import com.mss.iqra.service.ActivityTransactionService;

@Service
public class ActivityTransServiceImpl implements ActivityTransactionService {

	@Autowired
	ActivityTransactionRepo activityTransRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public ActivityTransactionDto createActivityTransaction(ActivityTransactionDto activityTransactionDto) {
		ActivityTransaction activityTransaction = dozerBeanMapper.map(activityTransactionDto,
				ActivityTransaction.class);
		return dozerBeanMapper.map(activityTransRepo.save(activityTransaction), ActivityTransactionDto.class);
	}

	@Override
	public ActivityTransactionDto getActivityTransactionById(Long id) {

		return dozerBeanMapper.map(activityTransRepo.findById(id), ActivityTransactionDto.class);
	}

	@Override
	public List<ActivityTransactionDto> getActivityTransactions() {
		List<ActivityTransaction> activityTransactions = activityTransRepo.findAll();

		return activityTransactions.stream().map(g -> dozerBeanMapper.map(g, ActivityTransactionDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public void deleteActivityTransactionById(Long id) {

		activityTransRepo.deleteById(id);
	}

}
