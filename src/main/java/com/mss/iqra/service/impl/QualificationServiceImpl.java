package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.QualificationDto;
import com.mss.iqra.model.Qualification;
import com.mss.iqra.repo.QualificationRepo;
import com.mss.iqra.service.QualificationService;

@Service
public class QualificationServiceImpl implements QualificationService {

	@Autowired
	QualificationRepo qualificationRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public QualificationDto createQualification(QualificationDto qualificationDto) {
		Qualification qualification = dozerBeanMapper.map(qualificationDto, Qualification.class);
		return dozerBeanMapper.map(qualificationRepo.save(qualification), QualificationDto.class);
	}

	@Override
	public QualificationDto getQualificationById(Long id) {

		return dozerBeanMapper.map(qualificationRepo.getOne(id), QualificationDto.class);
	}

	@Override
	public List<QualificationDto> getAllQualifications() {
		List<Qualification> qualifications = qualificationRepo.findAll();
		return qualifications.stream().map(g -> dozerBeanMapper.map(g, QualificationDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public void deleteQualification(Long id) {

		qualificationRepo.deleteById(id);

	}

}
