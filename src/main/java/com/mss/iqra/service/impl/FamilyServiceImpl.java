package com.mss.iqra.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.FamilyDto;
import com.mss.iqra.dto.TeamDto;
import com.mss.iqra.model.Family;
import com.mss.iqra.repo.FamilyRepo;
import com.mss.iqra.service.FamilyService;

@Service
public class FamilyServiceImpl implements FamilyService {

	@Autowired
	FamilyRepo familyRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public FamilyDto createFamily(FamilyDto familyDto) {
		Family family = new Family();
		String dateOfJoin = familyDto.getDateOfJoin();
		familyDto.setDateOfJoin(null);

		family = dozerBeanMapper.map(familyDto, Family.class);
		family.setDateOfJoin(LocalDate.parse(dateOfJoin));
		family.setCreatedDate(LocalDateTime.now());
		familyRepo.save(family);

		familyDto.setFamilyId(family.getFamilyId());
		familyDto.setDateOfJoin(dateOfJoin);
		familyDto.setCreatedDate(LocalDateTime.now().toString());
		return familyDto;
	}

	@Override
	public FamilyDto getFamilyById(Long id) {
		Family family = familyRepo.getOne(id);
		FamilyDto familyDto = null;

		if (family != null) {
			String dateofJoin = family.getDateOfJoin().toString();
			String createdDate = family.getCreatedDate().toString();

			family.setCreatedDate(null);
			family.setDateOfJoin(null);

			familyDto = dozerBeanMapper.map(family, FamilyDto.class);
			familyDto.setCreatedDate(createdDate);
			familyDto.setDateOfJoin(dateofJoin);
		}
		return familyDto;
	}

	@Override
	public List<FamilyDto> getAllFamilies() {
		List<Family> families = familyRepo.findAll();
		List<FamilyDto> familyDto = new ArrayList<>();
		if (families != null) {
			for (Family family : families) {
				familyDto.add(getFamilyById(family.getFamilyId()));
			}
		}
		return familyDto;
	}

	@Override
	public void deleteFamily(Long id) {

		familyRepo.deleteById(id);
	}

}
