package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.FamilyScoreDto;
import com.mss.iqra.model.FamilyScore;
import com.mss.iqra.repo.FamilyScoreRepo;
import com.mss.iqra.service.FamilyScoreService;

@Service
public class FamilyScoreServiceImpl implements FamilyScoreService {

	@Autowired
	FamilyScoreRepo familyScoreRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public FamilyScoreDto createFamilyScore(FamilyScoreDto familyScoreDto) {
		FamilyScore familyScore = dozerBeanMapper.map(familyScoreDto, FamilyScore.class);
		return dozerBeanMapper.map(familyScoreRepo.save(familyScore), FamilyScoreDto.class);
	}

	@Override
	public FamilyScoreDto getFamilyScore(Long id) {

		return dozerBeanMapper.map(familyScoreRepo.findById(id), FamilyScoreDto.class);
	}

	@Override
	public List<FamilyScoreDto> getFamilyScoreList() {
		List<FamilyScore> familyScoreList = familyScoreRepo.findAll();
		return familyScoreList.stream().map(g -> dozerBeanMapper.map(g, FamilyScoreDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public void deleteFamilyScoreById(Long id) {

		familyScoreRepo.deleteById(id);
	}

}
