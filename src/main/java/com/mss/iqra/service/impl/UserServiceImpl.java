package com.mss.iqra.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.FamilyDto;
import com.mss.iqra.dto.LoginDto;
import com.mss.iqra.dto.QualificationDto;
import com.mss.iqra.dto.TeamDto;
import com.mss.iqra.dto.UserDto;
import com.mss.iqra.model.Family;
import com.mss.iqra.model.Qualification;
import com.mss.iqra.model.Team;
import com.mss.iqra.model.User;
import com.mss.iqra.repo.FamilyRepo;
import com.mss.iqra.repo.QualificationRepo;
import com.mss.iqra.repo.TeamRepo;
import com.mss.iqra.repo.UserRepo;
import com.mss.iqra.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepo userRepo;

	@Autowired
	FamilyRepo familyRepo;

	@Autowired
	TeamRepo teamRepo;

	@Autowired
	QualificationRepo qualificationRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public UserDto createUser(UserDto userDto) {

		String englishDob = userDto.getEnglishDob();
		userDto.setEnglishDob(null);
		
		if(userDto.getEmailId() != null) {
			userDto.setLoginId(userDto.getEmailId());
		}
		
		User user = dozerBeanMapper.map(userDto, User.class);

		if (userDto.getUserId() == null) {
			user.setCreatedDate(LocalDateTime.now());
			userDto.setCreatedDate(LocalDateTime.now().toString());
		}
		user.setEnglishDob(LocalDate.parse(englishDob));
		if (userDto.getFamilyDto() != null) {
			user.setFamily(new Family(userDto.getFamilyDto().getFamilyId()));
		}
		if (userDto.getQualificationDto() != null) {
			user.setQualification(new Qualification(userDto.getQualificationDto().getQualificationId()));
		}
		if (userDto.getTeamDto() != null) {
			user.setTeam(new Team(userDto.getTeamDto().getTeamId()));
		}

		userRepo.save(user);
		userDto.setUserId(user.getUserId());
		userDto.setEnglishDob(englishDob);
		return userDto;
	}

	@Override
	public UserDto getUserById(Long id) {

		return dozerBeanMapper.map(userRepo.findById(id), UserDto.class);
	}

	@Override
	public List<UserDto> getAllUsers() {
		List<User> userList = userRepo.findAll();
		return userList.stream().map(g -> dozerBeanMapper.map(g, UserDto.class)).collect(Collectors.toList());
	}

	@Override
	public void deleteUserById(Long id) {

		userRepo.deleteById(id);
	}

	@Override
	public UserDto loginUser(LoginDto loginDto) throws Exception {

		UserDto userResp = null;
		User user = new User();

		if (StringUtils.isNotEmpty(loginDto.getUserName()) && loginDto.getPassword() != null) {
			user = userRepo.findByUserNameAndUserPassword(loginDto.getUserName(), loginDto.getPassword());

			if (user != null) {
				userResp = dozerBeanMapper.map(user, UserDto.class);
				if (user.getFamily() != null) {
					userResp.setFamilyDto(new FamilyDto(user.getFamily().getFamilyId()));
				}
				if (user.getTeam() != null) {
					userResp.setTeamDto(new TeamDto(user.getTeam().getTeamId()));
				}
				if (user.getQualification() != null) {
					userResp.setQualificationDto(new QualificationDto(user.getQualification().getQualificationId()));
				}
			} else {
				throw new Exception("UserName or Password is incorrect");
			}
		}
		return userResp;
	}
}
