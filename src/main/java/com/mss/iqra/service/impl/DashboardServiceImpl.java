package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.DashboardDto;
import com.mss.iqra.model.Dashboard;
import com.mss.iqra.repo.DashboardRepo;
import com.mss.iqra.service.DashboardService;
@Service
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	DashboardRepo dashboardRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public DashboardDto createDashboard(DashboardDto dashboardDto) {
		Dashboard dashboard = dozerBeanMapper.map(dashboardDto, Dashboard.class);
		return dozerBeanMapper.map(dashboardRepo.save(dashboard), DashboardDto.class);
	}

	@Override
	public DashboardDto getDashboardById(Long id) {

		return dozerBeanMapper.map(dashboardRepo.findById(id), DashboardDto.class);
	}

	@Override
	public List<DashboardDto> getDashboardList() {
		List<Dashboard> dashboardList = dashboardRepo.findAll();
		return dashboardList.stream().map(g -> dozerBeanMapper.map(g, DashboardDto.class)).collect(Collectors.toList());
	}

	@Override
	public void deleteDashboardById(Long id) {

		dashboardRepo.deleteById(id);

	}

}
