package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.TeamDto;
import com.mss.iqra.model.Team;
import com.mss.iqra.repo.TeamRepo;
import com.mss.iqra.service.TeamService;

@Service
public class TeamServiceImpl implements TeamService {

	@Autowired
	TeamRepo teamRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public TeamDto createTeam(TeamDto teamDto) {
		Team team = dozerBeanMapper.map(teamDto, Team.class);
		return dozerBeanMapper.map(teamRepo.save(team), TeamDto.class);
	}

	@Override
	public TeamDto getTeamById(Long id) {

		return dozerBeanMapper.map(teamRepo.getOne(id), TeamDto.class);
	}

	@Override
	public List<TeamDto> getAllTeams() {
		List<Team> teams = teamRepo.findAll();
		return teams.stream().map(g -> dozerBeanMapper.map(g, TeamDto.class)).collect(Collectors.toList());
	}

	@Override
	public void deleteTeam(Long id) {
		teamRepo.deleteById(id);
	}

}
