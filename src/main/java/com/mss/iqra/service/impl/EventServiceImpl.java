package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.EventDto;
import com.mss.iqra.model.Event;
import com.mss.iqra.repo.EventRepo;
import com.mss.iqra.service.EventService;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	EventRepo eventRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public EventDto createEvent(EventDto eventDto) {
		Event event = dozerBeanMapper.map(eventDto, Event.class);
		return dozerBeanMapper.map(eventRepo.save(event), EventDto.class);
	}

	@Override
	public EventDto getEventById(Long id) {
		return dozerBeanMapper.map(eventRepo.findById(id), EventDto.class);
	}

	@Override
	public List<EventDto> getEvents() {
		List<Event> eventList = eventRepo.findAll();

		return eventList.stream().map(g -> dozerBeanMapper.map(g, EventDto.class)).collect(Collectors.toList());
	}

	@Override
	public void deleteEventById(Long id) {
		eventRepo.deleteById(id);

	}

}
