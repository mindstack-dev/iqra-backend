package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.BadgeDto;
import com.mss.iqra.model.Badge;
import com.mss.iqra.repo.BadgeRepo;
import com.mss.iqra.service.BadgeService;
@Service
public class BadgeServiceImpl implements BadgeService {
	
	@Autowired
	BadgeRepo badgeRepo;
	
	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public BadgeDto createBadge(BadgeDto badgeDto) {
		Badge badge =dozerBeanMapper.map(badgeDto, Badge.class);
		return dozerBeanMapper.map(badgeRepo.save(badge), BadgeDto.class);
	}

	@Override
	public BadgeDto getBadgeById(Long id) {
		
		return dozerBeanMapper.map(badgeRepo.findById(id), BadgeDto.class);
	}

	@Override
	public List<BadgeDto> getBadgeList() {
		List<Badge> badgeList=badgeRepo.findAll();
		return badgeList.stream().map(g -> dozerBeanMapper.map(g, BadgeDto.class)).collect(Collectors.toList());
	}

	@Override
	public void deleteBadgeById(Long id) {
	
		badgeRepo.deleteById(id);

	}

}
