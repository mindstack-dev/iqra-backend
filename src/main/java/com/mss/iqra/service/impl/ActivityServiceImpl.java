package com.mss.iqra.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mss.iqra.dto.ActivityDto;
import com.mss.iqra.model.Activity;
import com.mss.iqra.repo.ActivityRepo;
import com.mss.iqra.service.ActivityService;

@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	ActivityRepo activityRepo;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	public ActivityDto createActivity(ActivityDto activityDto) {
		Activity activity = dozerBeanMapper.map(activityDto, Activity.class);
		return dozerBeanMapper.map(activityRepo.save(activity), ActivityDto.class);
	}

	@Override
	public ActivityDto getActivityById(Long id) {

		return dozerBeanMapper.map(activityRepo.getOne(id), ActivityDto.class);
	}

	@Override
	public List<ActivityDto> getAllActivities() {
		List<Activity> activities = activityRepo.findAll();
		return activities.stream().map(g -> dozerBeanMapper.map(g, ActivityDto.class)).collect(Collectors.toList());
	}

	@Override
	public void deleteActivity(Long id) {
		activityRepo.deleteById(id);

	}

}
