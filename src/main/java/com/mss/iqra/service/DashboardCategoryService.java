package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.DashboardCategoryDto;

public interface DashboardCategoryService {

	DashboardCategoryDto createDashboardCategory(DashboardCategoryDto dashboardCategoryDto);

	DashboardCategoryDto getDashboardById(Long id);

	List<DashboardCategoryDto> getDashboardList();

	void deleteDashboardCategoryById(Long id);

}
