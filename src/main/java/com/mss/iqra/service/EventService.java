package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.EventDto;

public interface EventService {

	EventDto createEvent(EventDto eventDto);

	EventDto getEventById(Long id);

	List<EventDto> getEvents();

	void deleteEventById(Long id);

}
