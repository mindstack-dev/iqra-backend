package com.mss.iqra.service;

import java.util.List;

import com.mss.iqra.dto.UserRoleDto;

public interface UserRoleService {

	UserRoleDto createUserRole(UserRoleDto userRoleDto);

	UserRoleDto getUserRoleById(Long id);

	List<UserRoleDto> getAllUserRoles();

	void deleteUserRole(Long id);

}
