package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mss.iqra.model.Family;

public interface FamilyRepo extends JpaRepository<Family, Long> {

}
