package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.Item;
@Repository
public interface ItemRepo extends JpaRepository<Item, Long> {

}
