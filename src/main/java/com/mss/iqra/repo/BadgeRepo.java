package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.Badge;
@Repository
public interface BadgeRepo extends JpaRepository<Badge, Long> {

}
