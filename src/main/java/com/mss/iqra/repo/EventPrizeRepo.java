package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.EventPrize;
@Repository
public interface EventPrizeRepo extends JpaRepository<EventPrize, Long> {

}
