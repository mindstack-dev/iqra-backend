package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.DashboardType;
@Repository
public interface DashboardTypeRepo extends JpaRepository<DashboardType, Long> {

}
