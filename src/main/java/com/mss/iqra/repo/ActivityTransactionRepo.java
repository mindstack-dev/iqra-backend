package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.ActivityTransaction;
@Repository
public interface ActivityTransactionRepo extends JpaRepository<ActivityTransaction, Long>{

}
