package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.Setting;
@Repository
public interface SettingRepo extends JpaRepository<Setting, Long> {

}
