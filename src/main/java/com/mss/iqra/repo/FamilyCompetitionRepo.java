package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.FamilyCompetition;
@Repository
public interface FamilyCompetitionRepo extends JpaRepository<FamilyCompetition, Long> {

}
