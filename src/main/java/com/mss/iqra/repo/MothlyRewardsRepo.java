package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.MonthlyRewards;
@Repository
public interface MothlyRewardsRepo extends JpaRepository<MonthlyRewards, Long> {

}
