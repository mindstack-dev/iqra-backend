package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.UserBadge;
@Repository
public interface UserBadgeRepo extends JpaRepository<UserBadge, Long> {

}
