package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mss.iqra.model.Qualification;

public interface QualificationRepo extends JpaRepository<Qualification, Long> {

}
