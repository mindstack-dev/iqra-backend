package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.ItemImage;
@Repository
public interface ItemImageRepo extends JpaRepository<ItemImage, Long> {

}
