package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.OrderDetail;
@Repository
public interface OrderDetailRepo extends JpaRepository<OrderDetail, Long> {

}
