package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.Wisdom;
@Repository
public interface WisdomRepo extends JpaRepository<Wisdom, Long> {

}
