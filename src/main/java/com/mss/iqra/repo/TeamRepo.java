package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mss.iqra.model.Team;

public interface TeamRepo extends JpaRepository<Team, Long> {

}
