package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mss.iqra.model.User;

public interface UserRepo extends JpaRepository<User, Long> {
	
	User findByUserNameAndUserPassword(String userName, String password);
	

}
