package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.Dashboard;
@Repository
public interface DashboardRepo extends JpaRepository<Dashboard, Long> {

}
