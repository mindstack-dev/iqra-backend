package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mss.iqra.model.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

}
