package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mss.iqra.model.Event;

@Repository
public interface EventRepo extends JpaRepository<Event, Long> {

}
