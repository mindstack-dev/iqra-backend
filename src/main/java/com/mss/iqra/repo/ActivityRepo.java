package com.mss.iqra.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mss.iqra.model.Activity;

public interface ActivityRepo extends JpaRepository<Activity, Long> {

}
