package com.mss.iqra.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name="users")
public class User {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long userId;
	
	@Column(nullable = false, length = 100)
	private String loginId;
	
	@Column(nullable = false, length = 100)
	private String userPassword;
	
	@Column(nullable = false, length = 100)
	private String userName;
	
	@ManyToOne
	@JoinColumn(name = "family_id",nullable = false)
	private Family family;
	
	@Column(length = 11,nullable = false)
	private String geneder;
	
	@Column(length = 50, nullable = false)
	private String islamicDob;
	
	@Column(nullable = false)
	private LocalDate englishDob;
	
	@Column(length = 100)
	private String emailId;
	
	@Column(length = 25)
	private String mobileNo;
	
	@Column
	private boolean isStudent;
	
	@ManyToOne
	@JoinColumn(name ="qualification_id",nullable = true)
	private Qualification qualification;
	
	@Column(length = 500)
	private String userAddress;
	
	@Column(length = 100)
	private String userCity;
	
	@Column(length = 10)
	private String userPostalCode;
	
	@Column(length = 500)
	private String hobbies;
	
	@ManyToOne
	@JoinColumn(name = "team_id",nullable = false)
	private Team team;
	
	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean isParticipate;
	
	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean isVolunteer;
	
	@Column(nullable = false, columnDefinition = "boolean default false")
	private boolean isAcknowledge;
	
	@ManyToOne
    @JoinColumn(name = "mentor_id", nullable = true)
	private User mentor;
	
	@ManyToOne
    @JoinColumn(name = "reviewer_id", nullable = true)
	private User reviewer;
	
	@Column
	private String photoFilename;
	
	@Column(nullable = false , columnDefinition = "boolean default true")
	private boolean isActive;
	
	@Column(nullable = false)
	private LocalDateTime createdDate;
	
	@Column(nullable = true, columnDefinition = "boolean default true")
	private boolean enableNotification;
	
	
	

}
