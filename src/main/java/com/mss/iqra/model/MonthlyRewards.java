package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
public class MonthlyRewards {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long monthlyRewardsId;

	@ManyToOne
	@JoinColumn(nullable = false, name = "user_id")
	private User user;

	@ManyToOne
	@JoinColumn(nullable = false, name = "family_id")
	private Family family;

	@ManyToOne
	@JoinColumn(nullable = false, name = "team_id")
	private Team team;

	@Column(nullable = false)
	private Integer totalActivityPoints;

	@Column(nullable = false)
	private Integer pointsAwarded;

	@Column(nullable = false, length = 15)
	private String awardedMonth;

	@Column(nullable = false, length = 4)
	private String awardedYear;

	@Column(nullable = false)
	private Integer carryfwdPoints;

	@Column(nullable = false)
	private Integer carryfwdCash;

	@Column(nullable = false)
	private Integer balanceCreditPoints;

	@Column(nullable = false)
	private Integer pointsRedeemedShop;

	@Column(nullable = false)
	private Integer pointsRedeemedCash;

	@Column(nullable = false)
	private Integer totalReductionPoints;

	@Column(nullable = false)
	private Integer cashPaid;

	@Column(nullable = false)
	private Integer balanceCash;

	@Column(nullable = false)
	private Integer balancePoints;

	@Column(nullable = false)
	private LocalDateTime createdDate;

}
