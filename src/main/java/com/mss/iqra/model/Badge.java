package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "badges")
public class Badge {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long badgeId;

	@Column
	private String badgeDescription;

	@ManyToOne
	@JoinColumn(name = "activity_id", nullable = false)
	private Activity activity;

	@Column
	private Integer activityPoints;

	@Column(length = 250)
	private String badgeImageLink;

	@Column(nullable = false, columnDefinition = "boolean default true")
	private boolean badgeStatus;

	@Column
	private LocalDateTime createdDate;

	@Column
	private LocalDateTime modifiedDate;

}
