package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "activities")
public class Activity {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long activityId;

	@Column(length = 100)
	private String activityName;

	@ManyToOne
	@JoinColumn(name = "parent_activity_id", nullable = true)
	private Activity parentActivity;

	@Column(length = 500)
	private String activityDesc;

	@Column
	private String activityImg;

	@Column
	private String activityRefrence;

	@Column
	private Integer activityLevel;

	@Column(nullable = true)
	private Integer rewardPoints;

	@ManyToOne
	@JoinColumn(name = "wisdom_id")
	private Wisdom wisdom;

	@Column
	private String unitOfMeasure;

	@Column
	private LocalDateTime startDate;

	@Column
	private LocalDateTime endDate;

	@Column(nullable = false, columnDefinition = "boolean default true")
	private boolean activityStatus;

	@Column
	private LocalDateTime createdDate;

}
