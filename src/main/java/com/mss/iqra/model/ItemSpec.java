package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "item_specs")
public class ItemSpec {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long itemSpecId;

	@ManyToOne
	@JoinColumn(name = "item_id")
	private Item item;

	@Column(length = 100)
	private String specDescription;

	@Column(length = 100)
	private String specValue;

	@Column(length = 20)
	private String displaySection;

	@Column
	private boolean status;

	@Column
	private LocalDateTime createdDate;

	@Column
	private LocalDateTime modifiedDate;

}
