package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "settings")
public class Setting {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long settingId;

	@Column(length = 25, nullable = false)
	private String settingKey;

	@Column(length = 26, nullable = false)
	private String settingValue;

	@Column(nullable = false, columnDefinition = "boolean default true")
	private boolean status;

	@Column(nullable = false)
	private LocalDateTime createdDate;

}
