package com.mss.iqra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "qualification")
public class Qualification {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long qualificationId;

	@Column(length = 25, unique = true, nullable = false)
	private String qualificationName;

	@Column
	private String qualificationDesc;
	
	public Qualification() {
		
	}

	public Qualification(Long id) {
		super();
		this.qualificationId = id;	
	}
}
