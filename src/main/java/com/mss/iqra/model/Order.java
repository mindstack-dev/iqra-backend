package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long orderId;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@Column
	private LocalDateTime orderDate;

	@Column
	private Integer orderValue;

	@Column
	private Integer pointsUsed;

	@Column(length = 20)
	private String orderStatus;

	@Column
	private LocalDateTime createdDate;

	@Column
	private LocalDateTime modifiedDate;

}
