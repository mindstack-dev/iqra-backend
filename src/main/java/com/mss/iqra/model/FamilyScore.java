package com.mss.iqra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "family_scores")
public class FamilyScore {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long familyPointsId;
	
	@OneToOne
	@JoinColumn(name = "family_id")
	private Family family;
	
	@Column
	private Integer totalPoints;
	
	@Column
	private Integer balancePoints;

}
