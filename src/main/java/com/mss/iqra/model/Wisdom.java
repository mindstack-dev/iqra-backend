package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "wisdom")
public class Wisdom {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long wisdomId;

	@Column(length = 100)
	private String wisdomName;

	@Column(length = 5000)
	private String wisdomDescription;

	@Column(length = 500)
	private String wisdomUrl;

	@Column(length = 250)
	private String wisdomImageLink;

	@Column
	private Boolean wisdomStatus;

	@Column(length = 250)
	private String wisdomHashTags;

	@Column(length = 500)
	private String wisdomRefrences;

	@Column
	private LocalDateTime createdDate;

	@Column
	private LocalDateTime modifiedDate;

}
