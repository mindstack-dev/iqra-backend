package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "items")
public class Item {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long itemId;

	@Column(nullable = true, length = 10)
	private String itemCode;

	@Column(length = 50)
	private String itemName;

	@Column(length = 250, nullable = true)
	private String itemShortDesc;

	@Column(length = 1000, nullable = true)
	private String itemLongDesc;

	@Column(length = 25, nullable = true)
	private String itemSize;

	@Column(length = 20, nullable = true)
	private String itemColor;

	@Column(length = 20, nullable = true)
	private String itemQuantity;

	@Column(length = 5000, nullable = true)
	private String itemOverview;

	@Column(length = 20, nullable = true)
	private String itemDeliveryTime;

	@Column
	private Integer itemPoints;

	@Column
	private Integer discount;

	@Column
	private boolean status;

	@Column(nullable = false)
	private LocalDateTime createdDate;

	@Column(nullable = false)
	private LocalDateTime modifiedDate;

}
