package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "activity_transaction")
public class ActivityTransaction {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long transactionId;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Column(length = 1)
	private String transactionStatus;
	
	@ManyToOne
	@JoinColumn(name = "activity_id", nullable = false)
	private Activity activity;
	
	@Column(length = 50)
	private String units;
	
	@Column(nullable = false)
	private Integer totalPoints;
	
	@Column(nullable = false)
	private LocalDateTime modifiedDate;

}
