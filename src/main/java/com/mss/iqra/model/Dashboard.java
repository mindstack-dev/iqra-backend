package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "dashboard")
public class Dashboard {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long dashbaordId;

	@ManyToOne
	@JoinColumn(name = "dashboard_type_id", nullable = false)
	DashboardType dashboardType;

	@ManyToOne
	@JoinColumn(name = "dashboard_category_id", nullable = false)
	DashboardCategory dashboardCategory;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	User user;

	@ManyToOne
	@JoinColumn(name = "activity_id", nullable = false)
	Activity activity;

	@Column
	private Integer activityPoints;

	@Column
	private Integer userPoints;

	@Column
	private LocalDateTime createdDate;

	@Column
	private LocalDateTime modifiedDate;

}
