package com.mss.iqra.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "families")
public class Family {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long familyId;

	@Column(length = 100, nullable = false)
	private String familyName;

	@Column(unique = true, length = 100, nullable = false)
	private String familyDisplayName;

	@Column(length = 100)
	private String headOfFamilyName;

	@Column(length = 500)
	private String familyAddress;

	@Column(length = 100)
	private String familyCity;

	@Column(length = 10)
	private String familyPostalCode;

	@Column(length = 25)
	private String familyPhone;

	@Column(length = 50)
	private String familyEmail;

	@Column
	private LocalDate dateOfJoin;
	
	@Column(nullable = false, columnDefinition = "boolean default true")
	private boolean familyStatus;

	@Column(nullable = false)
	private LocalDateTime createdDate;
	
	public Family() {
		
	}
	
	public Family(Long id) {
		super();
		this.familyId = id;
	}

}
