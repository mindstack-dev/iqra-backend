package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "user_badges")
public class UserBadge {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long userBadgeId;
	
	@ManyToOne
	@JoinColumn(name = "badge_id", nullable = false)
	private Badge badge;
	
	@Column(nullable = false ,columnDefinition = "boolean default false")
	private boolean badgeDelivered;
	
	@Column(nullable = false)
	private LocalDateTime awardedDate;
	
	@Column(nullable = true)
	private LocalDateTime badgeDeliveryDate;
	
	

}
