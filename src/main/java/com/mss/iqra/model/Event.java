package com.mss.iqra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "events")
public class Event {

	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long eventId;
	
	@Column(nullable = false, length = 100)
	private String eventName;
	
	@Column(length = 250)
	private String hashTags;
	
	@Column(length = 250)
	private String eventDescription;
	
	@Column
	private LocalDateTime startDate;
	
	@Column
	private LocalDateTime endDate;
	
	@Column
	private boolean eventStatus;
	
	@Column
	private Integer eventPoints;
	
	@Column
	private LocalDateTime createdDate;
	
}
