package com.mss.iqra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.FamilyScoreDto;
import com.mss.iqra.service.FamilyScoreService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class FamilyScoreController {
	
	@Autowired
	FamilyScoreService familyScoreService;
	
	@ApiOperation(value = "save FamilyScore", notes = "This api is used to save the FamilyScore", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/familyScore")
	public FamilyScoreDto saveFamilyScoreDto(@RequestBody FamilyScoreDto familyScoreDto) {
		return familyScoreService.createFamilyScore(familyScoreDto);

	}

	@ApiOperation(value = "get all FamilyScores", notes = "This api is used to get all the FamilyScores", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/familyScoreList")
	public List<FamilyScoreDto> getAllFamilyScores() {
		return familyScoreService.getFamilyScoreList();

	}

	@ApiOperation(value = "get FamilyScore by Id", notes = "This api is used to get the FamilyScore by Id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/familyScore/{id}")
	public FamilyScoreDto getFamilyScoreById(@PathVariable Long id) {

		return familyScoreService.getFamilyScore(id);
	}

	@ApiOperation(value = "delete FamilyScore by Id", notes = "This api is used to delete the FamilyScore by Id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/familyScore/{id}")
	public void deleteFamilyScoreById(@PathVariable Long id) {
		familyScoreService.deleteFamilyScoreById(id);
	}


}
