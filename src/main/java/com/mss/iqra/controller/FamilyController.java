package com.mss.iqra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.FamilyDto;
import com.mss.iqra.service.FamilyService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class FamilyController {

	@Autowired
	FamilyService familyService;

	@ApiOperation(value = "save Family", notes = "This api is used to save the Family", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/family")
	public FamilyDto saveFamilyDto(@RequestBody FamilyDto familyDto) {
		return familyService.createFamily(familyDto);

	}

	@ApiOperation(value = "get all Families", notes = "This api is used to get all the Families", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/families")
	public List<FamilyDto> getAllFamilies() {
		return familyService.getAllFamilies();

	}

	@ApiOperation(value = "get Family by Id", notes = "This api is used to get the Family by Id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/family/{id}")
	public FamilyDto getFamilyById(@PathVariable Long id) {

		return familyService.getFamilyById(id);
	}

	@ApiOperation(value = "delete Family by Id", notes = "This api is used to delete the Family by Id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/family/{id}")
	public void deleteFamilyById(@PathVariable Long id) {
		familyService.deleteFamily(id);
	}

}
