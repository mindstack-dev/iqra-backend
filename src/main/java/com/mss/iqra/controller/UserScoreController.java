package com.mss.iqra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.UserScoreDto;
import com.mss.iqra.service.UserScoreService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class UserScoreController {

	@Autowired
	UserScoreService userScoreService;

	@ApiOperation(value = "save UserScore", notes = "This api is used to save the UserScore", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/userScore")
	public UserScoreDto saveUserScoreDto(@RequestBody UserScoreDto userScoreDto) {
		return userScoreService.createUserScore(userScoreDto);

	}

	@ApiOperation(value = "get all UserScoreList", notes = "This api is used to get all the UserScoreList", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/userScoreList")
	public List<UserScoreDto> getAllUserScoreList() {
		return userScoreService.getUserScoreList();

	}

	@ApiOperation(value = "get UserScore by Id", notes = "This api is used to get the UserScore by Id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/userScore/{id}")
	public UserScoreDto getUserScoreById(@PathVariable Long id) {

		return userScoreService.getUserScoreById(id);
	}

	@ApiOperation(value = "delete UserScore by Id", notes = "This api is used to delete the UserScore by Id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/userScore/{id}")
	public void deleteUserScoreById(@PathVariable Long id) {
		userScoreService.deleteUserScoreById(id);
	}

}
