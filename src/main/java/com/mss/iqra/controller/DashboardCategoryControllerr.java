package com.mss.iqra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.BadgeDto;
import com.mss.iqra.dto.DashboardCategoryDto;
import com.mss.iqra.service.DashboardCategoryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class DashboardCategoryControllerr {

	@Autowired
	DashboardCategoryService dashboardCategoryService;

	@ApiOperation(value = "save DashboardCategory", notes = "This api is used to save the DashboardCategory", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/dashboardCategory")
	public DashboardCategoryDto saveDashboardCategoryDto(@RequestBody DashboardCategoryDto dasCategoryDto) {
		return dashboardCategoryService.createDashboardCategory(dasCategoryDto);

	}

	@ApiOperation(value = "get all DashboardCategories", notes = "This api is used to get all the DashboardCategories", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/dashboardCategories")
	public List<DashboardCategoryDto> getAllActivities() {
		return dashboardCategoryService.getDashboardList();

	}

	@ApiOperation(value = "get DashboardCategory by Id", notes = "This api is used to get the DashboardCategory by Id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/dashboardCategory/{id}")
	public DashboardCategoryDto getDashboardCategoryById(@PathVariable Long id) {

		return dashboardCategoryService.getDashboardById(id);
	}

	@ApiOperation(value = "delete DashboardCategory by Id", notes = "This api is used to delete the DashboardCategory by Id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/dashboardCategory/{id}")
	public void deleteDashboardCategoryById(@PathVariable Long id) {
		dashboardCategoryService.deleteDashboardCategoryById(id);
	}

}
