package com.mss.iqra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.ActivityTransactionDto;
import com.mss.iqra.service.ActivityTransactionService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class ActivityTransactionController {
	
	@Autowired
	ActivityTransactionService activityTransService;
	
	@ApiOperation(value = "save ActivityTransaction", notes = "This api is used to save the ActivityTransaction", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })
	
	@PostMapping("/activityTransaction")
	public ActivityTransactionDto saveActivityTransaction(ActivityTransactionDto activityTransactionDto) {
		
		return activityTransService.createActivityTransaction(activityTransactionDto);
	}
	
	@ApiOperation(value = "get ActivityTransaction", notes = "This api is used to get the ActivityTransaction by id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })
	
	@GetMapping("/activityTransaction/{id}")
	public ActivityTransactionDto getActivityTransactionById(Long id) {
		return activityTransService.getActivityTransactionById(id);
		
	}
	
	@ApiOperation(value = "get ActivityTransaction List", notes = "This api is used to get the ActivityTransactionList", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })
	
	@GetMapping("/activityTransactions")
	public List<ActivityTransactionDto> getActivityTransactionList(){
		return activityTransService.getActivityTransactions();
	}

	
	@ApiOperation(value = "delete ActivityTransaction", notes = "This api is used to delete the ActivityTransaction by id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/activityTansaction/{id}")
	public void deleteActivityTransaction(Long id) {
		 activityTransService.deleteActivityTransactionById(id);
	}

}
