package com.mss.iqra.controller;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.FamilyDto;
import com.mss.iqra.dto.LoginDto;
import com.mss.iqra.dto.QualificationDto;
import com.mss.iqra.dto.TeamDto;
import com.mss.iqra.dto.UserDto;
import com.mss.iqra.model.User;
import com.mss.iqra.repo.UserRepo;
import com.mss.iqra.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class UserController {

	@Autowired
	UserService userService;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@ApiOperation(value = "Login", notes = "This api is used to Login the User", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/login")
	public UserDto loginUser(@RequestBody LoginDto loginDto)throws Exception {
		return userService.loginUser(loginDto);
	}


	@ApiOperation(value = "save User", notes = "This api is used to save the User", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/user")
	public UserDto saveUserDto(@RequestBody UserDto userDto) {
		return userService.createUser(userDto);

	}

	@ApiOperation(value = "get all Users", notes = "This api is used to get all the Users", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/users")
	public List<UserDto> getAllUsers() {
		return userService.getAllUsers();

	}

	@ApiOperation(value = "get User by Id", notes = "This api is used to get the User by Id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/user/{id}")
	public UserDto getUserById(@PathVariable Long id) {

		return userService.getUserById(id);
	}

	@ApiOperation(value = "delete User by Id", notes = "This api is used to delete the User by Id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/user/{id}")
	public void deleteUserById(@PathVariable Long id) {
		userService.deleteUserById(id);
	}

}
