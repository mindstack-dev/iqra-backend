package com.mss.iqra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.DashboardDto;
import com.mss.iqra.service.DashboardService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class DashboardController {

	@Autowired
	DashboardService dashboardService;

	@ApiOperation(value = "save Dashboard", notes = "This api is used to save the Dashboard", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/dashboard")
	public DashboardDto saveDashboardDto(@RequestBody DashboardDto dashboardDto) {
		return dashboardService.createDashboard(dashboardDto);

	}

	@ApiOperation(value = "get all Dashboards", notes = "This api is used to get all the Dashboards", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/dashboards")
	public List<DashboardDto> getAllActivities() {
		return dashboardService.getDashboardList();

	}

	@ApiOperation(value = "get Dashboard by Id", notes = "This api is used to get the Dashboard by Id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/dashboard/{id}")
	public DashboardDto getDashboardById(@PathVariable Long id) {

		return dashboardService.getDashboardById(id);
	}

	@ApiOperation(value = "delete Dashboard by Id", notes = "This api is used to delete the Dashboard by Id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/dashboard/{id}")
	public void deleteDashboardById(@PathVariable Long id) {
		dashboardService.deleteDashboardById(id);
	}

}
