package com.mss.iqra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mss.iqra.dto.BadgeDto;
import com.mss.iqra.service.BadgeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/api/iqra")
public class BadgeController {
	
	@Autowired
	BadgeService badgeService;
	
	
	@ApiOperation(value = "save Badge", notes = "This api is used to save the Badge", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@PostMapping("/badge")
	public BadgeDto saveBadgeDto(@RequestBody BadgeDto badgeDto) {
		return badgeService.createBadge(badgeDto);

	}

	@ApiOperation(value = "get all Badges", notes = "This api is used to get all the Badges", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/badges")
	public List<BadgeDto> getAllActivities() {
		return badgeService.getBadgeList();

	}

	@ApiOperation(value = "get Badge by Id", notes = "This api is used to get the Badge by Id", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@GetMapping("/badge/{id}")
	public BadgeDto getBadgeById(@PathVariable Long id) {

		return badgeService.getBadgeById(id);
	}

	@ApiOperation(value = "delete Badge by Id", notes = "This api is used to delete the Badge by Id", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Internal Server error"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "UnAuthorized"),
			@ApiResponse(code = 404, message = "NOT_FOUND") })

	@DeleteMapping("/badge/{id}")
	public void deleteBadgeById(@PathVariable Long id) {
		badgeService.deleteBadgeById(id);
	}


}
