package com.mss.iqra.dto;

import lombok.Data;

@Data
public class SettingDto {

	private Long settingId;

	private String settingKey;

	private String settingValue;

	private boolean status;

	private String createdDate;

}
