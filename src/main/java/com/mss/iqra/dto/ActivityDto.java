package com.mss.iqra.dto;

import lombok.Data;

@Data
public class ActivityDto {

	private Long activityId;

	private String activityName;
	
	private ActivityDto parentActivity;

	private String activityDesc;

	private String activityImg;
	
	private String activityRefrence;
	
	private Integer activityLevel;

	private Integer rewardPoints;
	
	private String unitOfMeasure;
	
	private String startDate;
	
	private String endDate;
	
	private boolean activityStatus;
	
	private String createdDate;

}
