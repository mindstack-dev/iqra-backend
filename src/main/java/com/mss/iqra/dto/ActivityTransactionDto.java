package com.mss.iqra.dto;

import lombok.Data;

@Data
public class ActivityTransactionDto {
	
	private Long transactionId;

	private UserDto user;
	
	private String transactionStatus;

	private ActivityDto activity;
	
	private String units;

	private Integer totalPoints;

	private String modifiedDate;


}
