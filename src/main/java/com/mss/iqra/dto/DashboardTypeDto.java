package com.mss.iqra.dto;

import lombok.Data;

@Data
public class DashboardTypeDto {
	
	private Long dashboardTypeId;
	
	private String dashboardTypeName;

}
