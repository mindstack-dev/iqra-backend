package com.mss.iqra.dto;

import lombok.Data;

@Data
public class FamilyScoreDto {

	
	private Long familyPointsId;

	private FamilyDto family;
	
	private Integer totalPoints;

	private Integer balancePoints;

}
