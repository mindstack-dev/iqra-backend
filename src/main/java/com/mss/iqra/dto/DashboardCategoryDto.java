package com.mss.iqra.dto;

import lombok.Data;

@Data
public class DashboardCategoryDto {

	private Long dashboardCategoryId;
	
	private String dashboardCateggoryName;
}
