package com.mss.iqra.dto;

import lombok.Data;

@Data
public class UserScoreDto {
	
	private Long pointsId;

	private UserDto user;
	
	private Integer totalPoints;

	private Integer balancePoints;

	private Integer goldStars;

	private Integer silverstars;

	private Integer copperStars;

	private String misc;


}

