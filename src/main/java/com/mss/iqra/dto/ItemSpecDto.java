package com.mss.iqra.dto;

import lombok.Data;

@Data
public class ItemSpecDto {

	private Long itemSpecId;

	private ItemDto item;

	private String specDescription;

	private String specValue;

	private String displaySection;

	private boolean status;

	private String createdDate;

	private String modifiedDate;
}
