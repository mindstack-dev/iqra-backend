package com.mss.iqra.dto;

import lombok.Data;

@Data
public class ItemDto {

	private Long itemId;

	private String itemCode;

	private String itemName;

	private String itemShortDesc;

	private String itemLongDesc;

	private String itemSize;

	private String itemColor;

	private String itemQuantity;

	private String itemOverview;

	private String itemDeliveryTime;

	private Integer itemPoints;

	private Integer discount;

	private boolean status;

	private String createdDate;

	private String modifiedDate;
}
