package com.mss.iqra.dto;

import lombok.Data;

@Data
public class TeamDto {
	
	public TeamDto() {
		
	}

	public TeamDto(Long id) {
		// TODO Auto-generated constructor stub
		super();
		this.teamId = id;
	}

	private Long teamId;
	
	private String teamName;
	
	private String teamDesc;
	
}
