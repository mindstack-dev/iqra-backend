package com.mss.iqra.dto;

import lombok.Data;

@Data
public class QualificationDto {

	public QualificationDto() {

	}
	
	public QualificationDto(Long id) {
		super();
		this.qualificationId = id;
	}

	private Long qualificationId;

	private String qualificationName;

	private String qualificationDesc;
}
