package com.mss.iqra.dto;

import lombok.Data;

@Data
public class DashboardDto {

	private Long dashbaordId;

	private DashboardTypeDto dashboardType;

	private DashboardCategoryDto dashboardCategory;

	private UserDto user;

	private ActivityDto activity;

	private Integer activityPoints;

	private Integer userPoints;

	private String createdDate;

	private String modifiedDate;
}
