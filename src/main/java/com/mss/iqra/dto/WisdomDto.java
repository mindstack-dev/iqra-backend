package com.mss.iqra.dto;

import lombok.Data;

@Data
public class WisdomDto {

	private Long wisdomId;
	private String wisdomName;
	private String wisdomDescription;
	private String wisdomUrl;
	private String wisdomImageLink;
	private Boolean wisdomStatus;
	private String wisdomHashTags;
	private String wisdomRefrences;
	private String createdDate;
	private String modifiedDate;

}
