package com.mss.iqra.dto;

import lombok.Data;

@Data
public class OrderDto {

	private Long orderId;

	private UserDto user;

	private String orderDate;

	private Integer orderValue;

	private Integer pointsUsed;

	private String orderStatus;

	private String createdDate;

	private String modifiedDate;
}
