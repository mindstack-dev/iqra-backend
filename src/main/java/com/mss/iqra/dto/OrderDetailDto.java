package com.mss.iqra.dto;

import lombok.Data;

@Data
public class OrderDetailDto {

	private Long orderDetailId;

	private OrderDto order;

	private ItemDto item;

	private Integer quantity;

	private Integer unitPoints;

	private Integer lineItemPoints;
}
