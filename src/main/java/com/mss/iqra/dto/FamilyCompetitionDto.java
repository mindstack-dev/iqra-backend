package com.mss.iqra.dto;

import lombok.Data;

@Data
public class FamilyCompetitionDto {

	private Long familyCompetitionId;

	private FamilyDto familyDto;

	private EventDto eventDto;

	private Integer prizeNo;

	private String createdDate;
}
