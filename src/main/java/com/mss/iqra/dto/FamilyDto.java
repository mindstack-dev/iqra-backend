package com.mss.iqra.dto;

import lombok.Data;

@Data
public class FamilyDto {

	public FamilyDto() {
		
	}
	public FamilyDto(Long id) {
		// TODO Auto-generated constructor stub
		super();
		this.familyId = id;
	}

	private Long familyId;
	
	private String familyName;
	
	private String familyDisplayName;
	
	private String headOfFamilyName;
	
	private String familyAddress;
	
	private String familyCity;
	
	private String familyPostalCode;
	
	private String familyPhone;
	
	private String familyEmail;
	
	private String dateOfJoin;
	
	private boolean familyStatus;
	
	private String createdDate;
}
