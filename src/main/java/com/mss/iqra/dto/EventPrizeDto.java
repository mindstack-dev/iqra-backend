package com.mss.iqra.dto;

import lombok.Data;

@Data
public class EventPrizeDto {

	private Long eventPrizeId;

	private EventDto event;

	private Integer prizeNo;

	private boolean isCash;

	private Integer prizeAmount;

	private String prizeDescription;

}
