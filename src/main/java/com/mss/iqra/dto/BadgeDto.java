package com.mss.iqra.dto;

import lombok.Data;

@Data
public class BadgeDto {

	private Long badgeId;

	private String badgeDescription;

	private ActivityDto activityDto;

	private Integer activityPoints;

	private String badgeImageLink;

	private boolean badgeStatus;

	private String createdDate;

	private String modifiedDate;
}
