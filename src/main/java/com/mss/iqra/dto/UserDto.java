package com.mss.iqra.dto;

import lombok.Data;

@Data
public class UserDto {

	private Long userId;
	
	private String loginId;
	
	private String userPassword;
	
	private String userName;
	
	private FamilyDto familyDto;
	
	private String geneder;
	
	private String islamicDob;
	
	private String englishDob;
	
	private String emailId;
	
	private String mobileNo;
	
	private boolean isStudent;
	
	private QualificationDto qualificationDto;
	
	private String userAddress;
	
	private String userCity;
	
	private String userPostalCode;
	
	private String hobbies;
	
	private TeamDto teamDto;
	
	private boolean isParticipate;
	
	private boolean isVolunteer;
	
	private boolean isAcknowledge;
	
	private String photoFilename;
	
	private boolean isActive;
	
	private String createdDate;
	
	private boolean enableNotification;

	
}
