package com.mss.iqra.dto;

import lombok.Data;

@Data
public class UserRoleDto {
	
	private Long roleId;
	
	private String roleName;
	
	private String roleDesc;

}
