package com.mss.iqra.dto;

import lombok.Data;

@Data
public class OrderStatusHistoryDto {

	private Long orderStatusHistoryId;

	private OrderDto order;

	private String orderStaus;

	private String statusChangeDate;
}
