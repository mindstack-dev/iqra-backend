package com.mss.iqra.dto;

import lombok.Data;

@Data
public class MonthlyRewardsDto {

	private Long monthlyRewardsId;

	private UserDto user;

	private FamilyDto family;

	private TeamDto team;

	private Integer totalActivityPoints;

	private Integer pointsAwarded;

	private String awardedMonth;

	private String awardedYear;

	private Integer carryfwdPoints;

	private Integer carryfwdCash;

	private Integer balanceCreditPoints;

	private Integer pointsRedeemedShop;

	private Integer pointsRedeemedCash;

	private Integer totalReductionPoints;

	private Integer cashPaid;

	private Integer balanceCash;

	private Integer balancePoints;

	private String createdDate;
}
