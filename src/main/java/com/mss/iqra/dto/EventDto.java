package com.mss.iqra.dto;

import lombok.Data;

@Data
public class EventDto {
	
	private Long eventId;
	
	private String eventName;

	private String hashTags;

	private String eventDescription;
	
	private String startDate;
	
	private String endDate;
	
	private boolean eventStatus;

	private Integer eventPoints;

	private String createdDate;

}
