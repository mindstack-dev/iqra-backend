package com.mss.iqra.dto;

import lombok.Data;

@Data
public class ItemImageDto {

	private Long itemImageId;
	
	private ItemDto item;
	
	private String imageLink;
	
	private String imageType;
	
	private boolean isDefault;
	
	private boolean status;
	
	private String createdDate;

	private String modifiedDate;
}
