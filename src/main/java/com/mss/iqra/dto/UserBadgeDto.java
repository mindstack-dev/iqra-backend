package com.mss.iqra.dto;

import lombok.Data;

@Data
public class UserBadgeDto {

	private Long userBadgeId;

	private BadgeDto badge;

	private boolean badgeDelivered;

	private String awardedDate;

	private String badgeDeliveryDate;

}
