package com.mss.iqra.exception;

import javax.security.sasl.AuthenticationException;

public class UserAlreadyExistAuthenticationException extends AuthenticationException {

	    /**
		 * Default SerialVersionUID is added
		 */
		private static final long serialVersionUID = 1L;

		public UserAlreadyExistAuthenticationException(final String msg) {
	        super(msg);
	    }

}
