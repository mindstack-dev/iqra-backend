package com.mss.iqra.exception;


	public class ResourceNotFoundException extends RuntimeException {

		/**
		 * Default SerialVersionUID added
		 */
		private static final long serialVersionUID = 1L;
		private Long resourceId;
		private String uniqueId;

		public ResourceNotFoundException(Long resourceId, String message) {
			super(message);
			this.resourceId = resourceId;
		}

		public ResourceNotFoundException(String resourceId, String message) {
			super(message);
			this.uniqueId = resourceId;
		}

		public Long getResourceId() {
			return resourceId;
		}

		public void setResourceId(Long resourceId) {
			this.resourceId = resourceId;
		}

		public String getUniqueId() {
			return uniqueId;
		}

		public void setUniqueId(String uniqueId) {
			this.uniqueId = uniqueId;
		}


}
