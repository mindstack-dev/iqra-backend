package com.mss.iqra.exception;

import javax.security.sasl.AuthenticationException;

public class UserNotValidException extends AuthenticationException {

	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public UserNotValidException(final String msg) {
	        super(msg);
	    }

}
